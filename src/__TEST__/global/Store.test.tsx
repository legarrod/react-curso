import { reducer } from "global/Store";

const initialState = {};

it("should render reducer return default", () => {
  const result = reducer(initialState, { type: "default" });

  expect(result).toEqual(initialState);
});
